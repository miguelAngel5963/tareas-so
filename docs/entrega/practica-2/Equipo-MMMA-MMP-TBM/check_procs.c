#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <dirent.h>
#include <errno.h>

int main(int argc, char* argv[], char* envp[])
{
  /* Variables */
  int total_procs = 0;
  DIR* dirp;
  struct dirent * entry;

  char *p1;
  char *p2;
  int num_w = 0;
  int num_c = 0;
  errno = 0;

  /* Inicializa informacion del equipo */
  char TEAM[] = "Equipo-MMMA-MMP-TBM";
  char MARTÍNEZ[] = "314133225\tMiguel Ángel Martínez Mendoza";
  char MONTOYA[] = "312195362\tPedro Montoya Montes";
  char TORRES[] = "313284623\tMiriam Torres Bucio";
  char VERSION[] = "v0.0.1";

  /* 
  Si no se dan parametros de ayuda/version ni de procesos
  se lanza el mensaje con salida -1 
  */
  if (argc != 5)
  {
    printf("%s: Could not parse arguments\n", argv[0]);
    printf("Usage:\n\t%s [-w/--warning] NUM1 [-c/--critical] NUM2\n", argv[0]);
    return -1;
  }

  /*
  Si se dan 4 argumentos, se cuentan los procesos en /proc
  */
  dirp = opendir("/proc");
  while ((entry = readdir(dirp)) != NULL) {
    if (entry->d_type == DT_REG)
    {
      total_procs++;
    }
  }
  closedir(dirp);
  //printf("TOTAL:%d", total_procs);// TODO delete this debug line

  long conv1 = strtol(argv[2], &p1, 10);
  long conv2 = strtol(argv[4], &p2, 10);
  
  // Maneja si argv[2] o argv[4] no son enteros
  if (errno != 0 || *p1 != '\0' || *p2 != '\0')   
  {
    printf("FAILURE: procesos: %d\n", total_procs);
    return -4;
  }
  num_w = conv1;
  num_c = conv2;

  /* Obtiene parametros para procesar */

  /* Parametro de version */
  if (strcmp(argv[1], "-V") == 0 || strcmp(argv[1], "--version")==0)
  {
    printf("%s %s (Sistemas Operativos - %s)\n", argv[0], VERSION, TEAM);
    printf("%s\n%s\n%s\n", MARTÍNEZ, MONTOYA, TORRES);
    return -2;
  }
  /* Parametro de ayuda */
  else if (strcmp(argv[1], "-h" ) == 0 || strcmp(argv[1], "--help")==0)
  {
    printf("%s %s (Sistemas Operativos - %s)\n", argv[0], VERSION, TEAM);
    printf("%s\n%s\n%s\n", MARTÍNEZ, MONTOYA, TORRES);
    printf("\nThis plugin will simply return the state corresponding to the numeric value of the <state> argument with optional text");
    printf("\n\nUsage:\n\t <integer state> [optional text]\n");
    printf("\nOptions:\n\t-h, --help\n\t\tPrint detailed help screen\n\t-V, --version\n\t\tPrint version information\n");
    return -3;
  } 
  
  // Here handle args, ie: programa -w NUM -c NUM_C or programa -c NUM_C -w NUM
  if ((strcmp(argv[1], "-w" ) == 0 || strcmp(argv[1], "--warning") == 0) && (strcmp(argv[3], "-c" ) == 0 || strcmp(argv[3], "--critical")==0))
  {
    /* Parametros de entrada*/

    if (total_procs > 0 && total_procs < num_w)
    {
      printf("OK: procesos: %d\n", total_procs);
      return 0;
    }
    else if (total_procs >= num_w && total_procs < num_c)
    {
      printf("WARNING: procesos: %d\n", total_procs);
      return 1;
    }
    else if (num_c <= total_procs)
    {
      printf("CRITICAL: procesos: %d\n", total_procs);
      return 2;
    }    
    else if (total_procs == 0)
    {
      printf("UNKNOWN: procesos: %d\n", total_procs);
      return 3;
    }
    else
    {
      printf("FAILURE: procesos: %d\n", total_procs);
      return -4;
    }
  }

  else
  {
    printf("FAILURE: procesos: %d\n", total_procs);
    printf("FAILURE: Status %s or %s is not supported\n",argv[1],argv[3]);
    // Al ejecutar el comando ./programa con un numero negativo
    // seguido de echo $? éste nos regresa el complemento a 2
    // de -4, es decir 252
    return -4;
  }

  /* Código de salida */
  return EXIT_SUCCESS;
}

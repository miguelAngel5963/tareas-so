#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

int main(int argc, char* argv[], char* envp[])
{
  /* Inicializando buffer */
  size_t tamanio=71;
  char mensaje[tamanio];
  bzero(mensaje, tamanio);

  char* start_msg = "";

  /* Inicializa informacion del equipo */
  char TEAM[] = "Equipo-MMMA-MMP-TBM";
  char MARTÍNEZ[] = "314133225\tMiguel Ángel Martínez Mendoza";
  char MONTOYA[] = "312195362\tPedro Montoya Montes";
  char TORRES[] = "313284623\tMiriam Torres Bucio";
  char VERSION[] = "v0.0.1";

  /* Si no se dan parametros se lanza el mensaje con salida -1 */
  if (argc == 1 && getenv("MESSAGE") == NULL)
  {
    printf("%s: Could not parse arguments\n", argv[0]);
    printf("Usage:\n\t%s <integer state> [optional text]\n", argv[0]);
    return -1;
  }
  /* Obtiene parametros para procesar */

  /* Parametro de version */
  if (strcmp(argv[1], "-V") == 0 || strcmp(argv[1], "--version")==0)
  {
    printf("%s %s (Sistemas Operativos - %s)\n", argv[0], VERSION, TEAM);
    printf("%s\n%s\n%s\n", MARTÍNEZ, MONTOYA, TORRES);
    return -2;  
  }

  /* Parametro de ayuda */
  else if (strcmp(argv[1], "-h" ) == 0 || strcmp(argv[1], "--help")==0)
  {
    printf("%s %s (Sistemas Operativos - %s)\n", argv[0], VERSION, TEAM);
    printf("%s\n%s\n%s\n", MARTÍNEZ, MONTOYA, TORRES);
    printf("\nThis plugin will simply return the state corresponding to the numeric value of the <state> argument with optional text");
    printf("\n\nUsage:\n\t <integer state> [optional text]\n");
    printf("\nOptions:\n\t-h, --help\n\t\tPrint detailed help screen\n\t-V, --version\n\t\tPrint version information\n");
    return -3;
  }
    
  /* Parametro opcional */
  if (argv[2]!=NULL)
  {
    if (strcmp(argv[2],"-")==0)
    {
      scanf("%s", (char *) &mensaje);
      start_msg = ": ";
    }
    else
    {
      strncpy(mensaje, argv[2], sizeof(mensaje));
      start_msg = ": ";
    }
  }

  /* Si no existe mensaje en parametro, revisa en entorno por mensaje*/
  else if (getenv("MESSAGE") != NULL && argv[1] != NULL)
  {
    strncpy(mensaje, getenv("MESSAGE"), sizeof(mensaje));
    start_msg = ": ";
  }


  /* Parametros de entrada/salida sin y con mensaje */
  if (strcmp(argv[1],"0") == 0)
  {
    printf("OK%s%s\n",start_msg, mensaje);
    return 0;
  }
  else if (strcmp(argv[1],"1") == 0)
  {
    printf("WARNING%s%s\n",start_msg, mensaje);
    return 1;
  }
  else if (strcmp(argv[1],"2") == 0)
  {
    printf("CRITICAL%s%s\n",start_msg, mensaje);
    return 2;
  }
  else if (strcmp(argv[1],"3") == 0)
  {
    printf("UNKNOWN%s%s\n",start_msg, mensaje);
    return 3;
  }
  else
  {
    printf("FAILURE: Status %s is not supported\n",argv[1]);
    // Al ejecutar el comando ./programa con un numero negativo 
    // seguido de echo $? éste nos regresa el complemento a 2 
    // de -4, es decir 252 
    return -4;
  }

  /* Código de salida */
  return EXIT_SUCCESS;
}

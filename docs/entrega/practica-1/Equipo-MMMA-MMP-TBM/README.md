# Sistemas Operativos
## Practica 01 
### Alumnos:
- Martínez Mendoza Miguel Ángel
- Montoya Montes Pedro
- Torres Bucio Miriam

### Profesor: 
- Torres Rodriguez José Luis
### Ayudantes:
- Hernández Bermúdez Andrés Leonardo
- Martínez Balderas José Antonio

#### Compilación de software desde código fuente

##### Descarga del código fuente
Encontramos el programa en C en la siguiente liga: 
- *https://github.com/shnupta/bric*

Descargamos el archivo bric.tar.gz

Los pasos que seguimos fueron los siguientes:
- Creamos una carpeta para descargar el programa mediante el comando en consola:
		$ mkdir -vp ∼/src
- Nos colocamos en la carpeta que creamos con:
		$ cd !$
- Descargamos el archivo buc.tar.gz con:
		$ wget -c -nv -O buc.tar.gz "https://github.com/shnupta/bric"

| Terminal con mkdir y descarga |
|:-----------------------------:|
| ![](img/1.png)                |

##### Extracción de código fuente
- Descomprimimos el archivo buc.tar.gz
		$ tar -xvvf buc.tar.gz
- Cambiamos de directorio para inspeccionar el contenido.
		$ cd buc/
		$ ls -A

##### Generar el Makefile
En nuestro caso, no fue necesario usar autoconf para generar el script configure, pues
ya venía en el código fuente. A sí que al ejecutar los mandos en consola nos dimos cuenta de ello.

		$ ls -A configure∗
		$ autoconf
		$ ls -A configure∗
		
| Terminal con autoconfigure |
|:--------------------------:|
| ![](img/2.png)             |


##### Configurar el programa
Pusimos el siguiente comando en consola:

		$ ./configure --help 2>&1 | tee configure.help

| Terminal con configure (1/4) |
|:----------------------------:|
| ![](img/3.png)               |

| Terminal con configure (2/4) |
|:----------------------------:|
| ![](img/4.jpeg)              |
		
		$ ./configure --prefix=$HOME/local

| Terminal con configure (3/4) |
|:----------------------------:|
| ![](img/5.png)               |

| Terminal con configure (4/4) |
|:----------------------------:|
| ![](img/6.jpeg)              |

##### Manejo de dependencias 
Antes de hacer cualquier otra cosa, instalamos las dependencias necesarias para que
no hubiera ningún problema en las próximas ejecuciones. Así que instalamos los paquetes *∗-devel y ∗-dev.*

#### Compilación del programa 

Para la compilación del programa, lo que hicimos fue hacer un archivo script.sh el cual contiene el siguiente código:

		#!/bin/bash
		echo "script para instalar x"
		mkdir -vp ${HOME}/src
		cd ${HOME}/src/
		wget -c -nv -O bric.tar.gz
		"https://github.com/shnupta/bric/archive/refs/tags/v0.0.2.tar.gz"
		tar -xvvf bric.tar.gz
		cd bric-0.0.2/
		make
		read -s -p "Introduce contraseña de sudo: " sudoPW
		echo $sudoPW | sudo make install
		echo $sudoPW | sudo tee -a ${HOME}/../../etc/bash.bashrc
		<<< 'export PATH="${HOME}/local/bin:${PATH}"'

- Para ejecutar el script.sh ponemos el siguiente comando en consola:

		$ chmod +x script.sh
		$ ./script.sh

Esto lo que hace es instalar los paquetes.

|  Ejecutando make  |
|:-----------------:|
| ![](img/make.png) |

| Makefile finalizado |
|:-------------------:|
| ![](img/9.jpeg)     |


|  Bric en consola  |
|:-----------------:|
| ![](img/11.jpeg)  |

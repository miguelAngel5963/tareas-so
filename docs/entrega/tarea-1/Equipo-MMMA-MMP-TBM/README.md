# Sistemas Operativos
## Tarea 01
### Alumnos:
* Martínez Mendoza Miguel Ángel
* Montoya Montes Pedro
* Torres Bucio Miriam
### Profesor:
* Torres Rodriguez José Luis
### Ayudantes:
* Hernández Bermúdez Andrés Leonardo
* Martínez Balderas José Antonio
## Makefile
Para la compilación y ejecución de este programa basta con abrir una terminal en la carpeta donde se encuentra el archivo "Makefile", abrir una terminal y ejecutar el comando "make".
### Compilar y ejecutar a mano
* ejecutar
```bash
gcc programa.c -o programa
```
```bash
MESSAGE="Mi mensaje :)" ./programa n
```
donde n es un numero entero entre 1 y 4 según las especificaciones de la tarea

#### Observaciones
* Al ejecutar
```bash
./programa -1; echo $?
```
este regresa el código -4, sin embargo nos lo regresa en complemento a 2 como un numero entero positivo, en este caso 252.
* Al ejecutar
```bash
MESSAGE="Mi mensaje :)" ./programa n
```
es necesario poner el parámetro obligatorio n para evitar el mensaje "Segmentation fault".

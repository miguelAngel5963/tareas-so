#!/bin/bash
# Mensaje breve de uso del programa
echo "--- Prueba programa sin argumentos ---"
./programa
echo $? 
# Version del programa
echo "--- Prueba version del programa ---"
./programa -V
echo $? 
./to_test --version
echo $? 
# Funcion de ayuda
echo "--- Prueba funcion de ayuda del programa ---"
./programa -h
echo $? 
./programa --help
echo $? 
# Procesamiento de argumentos
echo "----- Prueba procesamiento de argumentos -----"
echo "--- Prueba procesamiento de argumentos (sin mensaje) ---"
# Sin mensaje
./programa -1
echo $? 
./programa 0
echo $? 
./programa 1
echo $? 
./programa 2
echo $? 
./programa 3
echo $? 
./programa 4
echo $? 
# Mensaje desde argumento
echo "--- Prueba procesamiento de argumentos (mensaje desde argumento) ---"
./programa -1 "Test"
echo $? 
./programa 0 "Test"
echo $? 
./programa 1 "Test"
echo $? 
./programa 2 "Test"
echo $? 
./programa 3 "Test"
echo $? 
./programa 4 "Test"
echo $? 
# Mensaje desde variable de entorno
echo "--- Prueba procesamiento de argumentos (mensaje desde variable de entorno) ---"
MESSAGE="Test" ./programa -1
echo $?
MESSAGE="Test" ./programa 0
echo $?
MESSAGE="Test" ./programa 1
echo $?
MESSAGE="Test" ./programa 2
echo $?
MESSAGE="Test" ./programa 3
echo $?
MESSAGE="Test" ./programa 4
echo $?
# Mensaje desde entrada estandar
echo "--- Prueba procesamiento de argumentos (mensaje desde entrada estandar) ---"
echo "Test" | ./programa -1 -
echo $?
echo "Test" | ./programa 0 -
echo $?
echo "Test" | ./programa 1 -
echo $?
echo "Test" | ./programa 2 -
echo $?
echo "Test" | ./programa 3 -
echo $?
echo "Test" | ./programa 4 -
echo $?
# Sistemas Operativos
## Tarea 01
### Alumnos:
* Martínez Mendoza Miguel Ángel
* Montoya Montes Pedro
* Torres Bucio Miriam
### Profesor:
* Torres Rodriguez José Luis
### Ayudantes:
* Hernández Bermúdez Andrés Leonardo
* Martínez Balderas José Antonio
#### BITÁCORA DE TRABAJO
* 3 de Noviembre 
Se comenzó a discutir la practica con todo el equipo, se empezaron a 
hacer pequeños programas de prueba
* 4 de Noviembre 
se agregaron las funcionas básicas al archivo Makefile y se empezó a 
trabajar el script.sh que mas tarde se renombró como "test.sh" pues como
su nombre lo dice es el archivo que ejecutará todas las pruebas.
* 5 de Noviembre
Se limpió el código en general, se agregaron todas las distintas opciones
en las que se puede recibir el mensaje, tambien se terminan los archivos 
Makefile y test.sh, tambien se creó toda la estructura de las carpetas.
* 6 de Noviembre 
Este día se tuvo una reunión mediante meet para hacer una prueba general
del programa y revisar posibles errores, en este punto nos dimos cuenta de
ciertos detalles los cuales ya están reportados en el archivo README.md,
de igual manera se corriieron errores de escritura. 
